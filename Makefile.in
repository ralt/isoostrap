NAME = isoostrap
LISPCC ?= sbcl --no-userinit --no-sysinit
BUILD_PREFIX = build
BIN = $(BUILD_PREFIX)/$(NAME)
MAN = $(BUILD_PREFIX)/README.1.gz
BUILDAPP = $(BUILD_PREFIX)/buildapp
QUICKLISP = $(BUILD_PREFIX)/quicklisp
MANIFEST = $(BUILD_PREFIX)/manifest
SRC := $(wildcard src/*.lisp)
ASD := $(wildcard *.asd)
PREFIX = ##PREFIX##
SHASUM ?= shasum

.PHONY: all clean install

all: $(BIN) $(MAN)

$(BIN): $(BUILDAPP) $(MANIFEST) $(SRC)
	$(BUILDAPP) \
		--manifest-file $(MANIFEST) \
		--load-system $(NAME) \
		--output $@ \
		--entry $(NAME)::main

$(MAN): README.md
	mkdir -p $(BUILD_PREFIX)
	pandoc -s -t man README.md | gzip -9 > $@

clean:
	rm -rf $(BUILD_PREFIX)

install:
	install --directory $(DESTDIR)$(PREFIX)/bin
	install --mode=755 $(BIN) $(DESTDIR)$(PREFIX)/bin/

	install --directory $(DESTDIR)$(PREFIX)/share/man/man1/
	install --mode=755 $(MAN) $(DESTDIR)$(PREFIX)/share/man/man1/
	mandb > /dev/null

$(MANIFEST): $(QUICKLISP)/setup.lisp $(ASD)
	$(LISPCC) \
		--load $< \
		--eval '(push "$(PWD)/" asdf:*central-registry*)' \
		--eval '(ql:quickload :$(NAME))' \
		--eval '(ql:write-asdf-manifest-file "$(MANIFEST)")' \
		--eval '(quit)'
	echo "$(PWD)"/isoostrap.asd >> $(MANIFEST)

$(BUILDAPP): $(QUICKLISP)/setup.lisp
	$(LISPCC) \
		--load $< \
		--eval "(setf *debugger-hook* #'(lambda (c h) (declare (ignore h)) (print c) (abort)))" \
		--eval '(ql:quickload :buildapp)' \
		--eval '(buildapp:build-buildapp "$@")' \
		--eval '(quit)'

$(QUICKLISP)/setup.lisp: $(BUILD_PREFIX)/quicklisp.lisp
	$(LISPCC) \
		--load $< \
		--eval '(quicklisp-quickstart:install :path "$(QUICKLISP)/")' \
		--eval '(quit)'

$(BUILD_PREFIX)/quicklisp.lisp:
	mkdir -p $(BUILD_PREFIX)
	cd $(BUILD_PREFIX) && wget https://beta.quicklisp.org/quicklisp.lisp || \
	curl -O  https://beta.quicklisp.org/quicklisp.lisp
	echo '4a7a5c2aebe0716417047854267397e24a44d0cce096127411e9ce9ccfeb2c17 *$@' | $(SHASUM) -c -
