(defsystem #:isoostrap
  :description "Bootstrap ISOs like no tomorrow"
  :license "GPLv2"
  :author "Florian Margaine <florian@margaine.com>"
  :depends-on (:bordeaux-threads
               :cffi
               :cl-ppcre
               :command-line-arguments
               :drakma
               :local-time
               :uiop
               :usocket-server)
  :components ((:module
                "src"
                :components
                ((:file "isoostrap" :depends-on ("isoo"
                                                 "util"))
                 (:file "util")
                 (:file "isoo" :depends-on ("util"))
                 (:file "debian" :depends-on ("isoo"
                                              "util"))))))
