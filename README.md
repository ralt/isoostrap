% ISOOSTRAP(1) Isoostrap User Manual
% Florian Margaine <florian@margaine.com>
% September 10, 2017

# NAME

isoostrap - bootstrap ISOs like no tomorrow

# SYNOPSIS

isoostrap [*OPTIONS*]...

# DESCRIPTION

The end goal of isoostrap is to let you build a root filesystem of any
distribution, which are installed through an ISO.

# OPTIONS

--iso *FILE*
:   The ISO file to boot on.

--output *FILE*
:   The file where to dump the built root filesystem.

--output-format *FORMAT*
:   The output format of the dump.  Supported formats: 'targz'
    (default), 'tarxz', 'tarbz2'.

--yes
:   Enable the non-interactive mode.

--help
:   Show usage message.

# SAUCE

isoostrap works by using a qemu virtual machine to boot on an ISO
image, and saves the installed system into an archive.

# LICENSE

This software is licensed under the MIT license.

# SEE ALSO

The isoostrap source code and all documentation may be downloaded
from <https://gitlab.com/ralt/isoostrap>.
