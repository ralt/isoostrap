(in-package #:isoostrap/user)

(defvar *debian-command-line-spec*
  '((("post-install")
     :type string
     :optional t
     :documentation "Path to script to run at the end of the installation.")
    (("hostname")
     :type string
     :optional t
     :documentation "Hostname of the machine. Defaults to 'debian'.")
    (("suite")
     :type string
     :optional t
     :documentation "Suite to install. Defaults to 'stretch'. (Downgrades from the ISO are not supported.)")
    (("root-password")
     :type string
     :optional t
     :documentation "The root password. Defaults to 'root'.")
    (("user-username")
     :type string
     :optional t
     :documentation "Username of the normal user account. Defaults to none.")
    (("user-password")
     :type string
     :optional t
     :documentation "Password of the normal user account, if applicable. Defaults to the username.")
    (("tasksel")
     :type string
     :optional t
     :documentation "Comma-separated list of tasks to select. Defaults to 'standard'.")
    (("no-kernel")
     :type boolean
     :optional t
     :documentation "Whether to install the kernel or not. Defaults to false.")
    (("packages")
     :type string
     :optional t
     :documentation "Comma-separated list of additional packages to install. Defaults to none.")
    (("cache")
     :type string
     :optional t
     :documentation "Folder to use as a Debian repositories cache. No cache is used when this argument isn't defined.")
    (("help")
     :type boolean
     :optional t
     :documentation "Show usage message.")))

(defclass debian (isoostrap/isoo:isoo)
  ((isoostrap/isoo:name :initform "isoo:debian")
   (isoostrap/isoo:command-line-spec :initform *debian-command-line-spec*)
   (post-install :accessor post-install)
   (hostname :accessor hostname)
   (suite :accessor suite)
   (root-password :accessor root-password)
   (user-username :accessor user-username)
   (user-password :accessor user-password)
   (no-kernel :accessor no-kernel)
   (packages :accessor packages)
   (cache :accessor cache)
   (tasksel :accessor tasksel)))

(defmethod isoostrap/isoo:handle-arguments ((isoo debian))
  (let ()
    (lambda (&key post-install hostname suite root-password user-username user-password
               no-kernel tasksel packages cache help)
      (block root
        (when help
          (return-from root
            (progn
              (isoostrap/util:help
               (with-output-to-string (s)
                 (command-line-arguments:show-option-help *debian-command-line-spec*
                                                          :stream s)
                 s))
              (uiop:quit 0))))

        (setf (post-install isoo) (if post-install
                                      (uiop:read-file-string post-install)
                                      ""))
        (setf (hostname isoo) (or hostname "debian"))
        (setf (suite isoo) (or suite "stretch"))
        (setf (root-password isoo) (or root-password "root"))
        (setf (user-username isoo) user-username)
        (setf (user-password isoo) (or user-password user-username))
        (setf (no-kernel isoo) no-kernel)
        (setf (tasksel isoo) (or tasksel "standard"))
        (setf (packages isoo) (ppcre:split "," packages))
        (setf (cache isoo) (string-right-trim '(#\/) cache))))))

(defmethod isoostrap/isoo:start-virtual-machine ((isoo debian) image &key)
  (isoostrap/util:with-temporary-directory (mounted-iso)
    (isoostrap/util:with-mounted-iso ((isoostrap/isoo:iso isoo) mounted-iso)
      (let ((ip-address "169.254.169.254"))
        (isoostrap/util:with-ip-address ("lo" ip-address)
          (multiple-value-bind (thread server)
              (usocket:socket-server ip-address 80 (debian-handler isoo) nil
                                     :in-new-thread t
                                     :multi-threading t
                                     :element-type '(unsigned-byte 8)
                                     :reuse-address t)
            (declare (ignore thread))
            (unwind-protect
                 (call-next-method
                  isoo
                  image
                  :additional-parameters
                  (list
                   "-kernel"
                   (namestring
                    (merge-pathnames #P"install.amd/vmlinuz"
                                     mounted-iso))
                   "-initrd"
                   (namestring
                    (merge-pathnames #P"install.amd/initrd.gz"
                                     mounted-iso))
                   "-append"
                   (format nil
                           "auto=true priority=critical preseed/url=http://~a/preseed.cfg"
                           ip-address)))
              (usocket:socket-close server))))))))

(defun debian-handler (isoo)
  (let ()
    (lambda (stream)
      ;; A mini HTTP server.
      (let* ((http-status-line (babel:octets-to-string (coerce
                                                        (loop
                                                           :for byte = (read-byte stream)
                                                           :until (= byte (char-code #\Newline))
                                                           :collect byte)
                                                        '(vector (unsigned-byte 8)))))
             (http-path (second (ppcre:split " " http-status-line))))
        (cond ((string= http-path "/preseed.cfg")
               (progn
                 (isoostrap/util:http-response stream (debian-preseed isoo))
                 (debian-debug "200 OK /preseed.cfg HIT")))
              ((string= http-path "/post-install.sh")
               (progn
                 (isoostrap/util:http-response stream (post-install isoo))
                 (debian-debug "200 OK /post-install.sh HIT")))
              ((isoostrap/util:starts-with http-path "/debian")
               (debian-proxy isoo http-path stream))
              (t (progn
                   (isoostrap/util:http-response stream
                                                 (format nil "Fake 404 because lazy~%"))
                   (debian-debug "404 Not found ~a MISS" http-path))))))))

(defun debian-debug (string &rest args)
  (apply #'format
         t
         (isoostrap/util:cat (local-time:format-timestring nil (local-time:now)) " " string "~%")
         args))

(defun debian-preseed (isoo)
  ;; A random preseed file to see if it works.
  (isoostrap/util:cat "
# Locale
d-i debian-installer/locale string en_US.UTF-8
d-i debian-installer/keymap select us
d-i keymap select us
d-i keyboard-configuration/xkb-keymap select us

# Network
d-i netcfg/choose_interface select auto
d-i netcfg/hostname string " (hostname isoo) "

# APT mirror
d-i mirror/country string manual
d-i mirror/http/hostname string 169.254.169.254
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string
d-i mirror/suite string " (suite isoo) "
d-i mirror/udeb/suite string " (suite isoo) "

# Account
d-i passwd/root-login boolean true
d-i passwd/make-user boolean " (if (user-username isoo) "true" "false") "
" (when (user-username isoo) (format nil "d-i passwd/user-fullname string ~A
d-i passwd/username string ~a
d-i passwd/user-password password ~a
d-i passwd/user-password-again password ~a" (user-username isoo) (user-username isoo) (user-password isoo) (user-password isoo))) "
d-i passwd/root-password password " (root-password isoo) "
d-i passwd/root-password-again password " (root-password isoo) "

# Clock
d-i clock-setup/utc boolean true
d-i time/zone string UTC
d-i clock-setup/ntp boolean true

# Partitioning
d-i partman-auto/init_automatically_partition select biggest_free
d-i partman-auto/method string regular
d-i partman-auto/choose_recipe select atomic
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true
d-i partman/mount_style select uuid

" (when (no-kernel isoo) "d-i base-installer/kernel/image string none") "

# APT packages
d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true
tasksel tasksel/first multiselect " (tasksel isoo) "
d-i pkgsel/include string " (format nil "~{~A~^, ~}" (packages isoo)) "
d-i pkgsel/upgrade select full-upgrade
popularity-contest popularity-contest/participate boolean false

# Custom script
d-i preseed/late_command string \\
in-target wget http://169.254.169.254/post-install.sh; \\
in-target chmod +x post-install.sh; \\
in-target ./post-install.sh; \\
in-target rm -f post-install.sh

# Grub
d-i grub-installer/only_debian boolean true
d-i grub-installer/with_other_os boolean true
d-i grub-installer/bootdev string default

# Finish
d-i finish-install/reboot_in_progress note
d-i finish-install/keep-consoles boolean true
d-i debian-installer/exit/poweroff boolean true
"))

(defvar *debian-locks* (make-hash-table :test #'equal))

(defun debian-proxy (isoo http-path http-stream)
  (isoostrap/util:with-hash-lock (*debian-locks* http-path)
    (when (and (cache isoo) (debian-proxy-local-file isoo http-path http-stream))
      (debian-debug "200 OK ~a HIT" http-path)
      (return-from debian-proxy))

    (debian-proxy-remote-file isoo http-path http-stream)))

(defun debian-proxy-local-file (isoo http-path http-stream)
  ;; This is completely not multi-process safe. But isoostrap
  ;; already isn't because of the network setup anyway, so don't
  ;; bother for now.
  (let ((cache-path (uiop:parse-native-namestring
                     (isoostrap/util:cat (cache isoo) http-path))))
    (ensure-directories-exist (make-pathname :directory (pathname-directory cache-path)))
    (when (probe-file cache-path)
      (with-open-file (f cache-path :element-type '(unsigned-byte 8))
        (let ((headers (babel:string-to-octets
                        (format
                         nil
                         "HTTP/1.1 200 OK~c~cHost: 169.254.169.254~c~cContent-Length: ~a~c~cConnection: close~c~c~c~c"
                         #\Return #\Linefeed
                         #\Return #\Linefeed
                         (file-length f)
                         #\Return #\Linefeed
                         #\Return #\Linefeed
                         #\Return #\Linefeed))))
          (write-sequence headers http-stream))
        (loop
           (let* ((buffer (make-array 8192 :element-type '(unsigned-byte 8)))
                  (read-bytes (read-sequence buffer f)))
             (write-sequence (subseq buffer 0 read-bytes) http-stream)
             (when (< read-bytes 8192)
               (return-from debian-proxy-local-file t))))))))

(defun debian-proxy-remote-file (isoo http-path http-stream)
  (multiple-value-bind (stream status-code headers parsed-uri socket should-be-closed reason)
      (drakma:http-request (isoostrap/util:cat "http://cdn-fastly.deb.debian.org" http-path)
                           :want-stream t)
    (declare (ignore parsed-uri socket should-be-closed))
    (unwind-protect
         (progn
           (write-sequence (babel:string-to-octets
                            (format nil
                                    "HTTP/1.1 ~a ~a~c~cHost: 169.254.169.254~c~c~{~a~}~c~c"
                                    status-code
                                    reason
                                    #\Return #\Linefeed
                                    #\Return #\Linefeed
                                    (mapcar (lambda (header)
                                              (format nil
                                                      "~a: ~a~c~c"
                                                      (car header)
                                                      (cdr header)
                                                      #\Return #\Linefeed))
                                            (remove-if (lambda (header)
                                                         (eq (car header) :host))
                                                       headers))
                                    #\Return #\Linefeed))
                           http-stream)
           (unless (= status-code 200)
             ;; Still return whatever body there was, but don't cache.
             (loop
                (let* ((buffer (make-array 4096 :element-type '(unsigned-byte 8)))
                       (read-bytes (read-sequence buffer (flexi-streams:flexi-stream-stream stream))))
                  (when (= read-bytes 0)
                    (return-from debian-proxy-remote-file
                      (debian-debug "~a ~a ~a BYPASS" status-code reason http-path)))
                  (write-sequence (subseq buffer 0 read-bytes) http-stream))))
           ;; Only cache the pool files, so that we're always up-to-date.
           (let* ((should-cache (and (cache isoo) (isoostrap/util:starts-with http-path
                                                                              "/debian/pool/")))
                  (cache-file-stream (when should-cache
                                      (let ((cache-file-path
                                             (uiop:parse-native-namestring
                                              (isoostrap/util:cat (cache isoo) http-path))))
                                        (ensure-directories-exist (make-pathname
                                                                   :directory (pathname-directory
                                                                               cache-file-path)))
                                        (open cache-file-path
                                              :direction :output
                                              :if-does-not-exist :create
                                              :element-type '(unsigned-byte 8))))))
             (unwind-protect
                  (loop
                     (let* ((buffer (make-array 4096 :element-type '(unsigned-byte 8)))
                            (read-bytes (read-sequence buffer
                                                       (flexi-streams:flexi-stream-stream stream))))
                       (when (= read-bytes 0)
                         (return-from debian-proxy-remote-file
                           (debian-debug "200 OK ~a ~a" http-path (if should-cache "MISS" "BYPASS"))))
                       (write-sequence (subseq buffer 0 read-bytes) http-stream)
                       (when should-cache
                         (write-sequence (subseq buffer 0 read-bytes) cache-file-stream))))
               (when should-cache
                 (close cache-file-stream)))))
      (close stream))))
