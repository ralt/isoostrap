(defpackage #:isoostrap/isoo
  (:use #:cl #:isoostrap/util)
  (:export arguments
           build
           command-line-spec
           create-archive
           create-temporary-image
           handle-arguments
           iso
           isoo
           load-all
           name
           start-virtual-machine
           output
           output-format
           yes

           *supported-formats*))

(in-package #:isoostrap/isoo)

(defvar *supported-formats*
  '(("targz" "z")
    ("tarxz" "J")
    ("tarbz2" "j")))

(defclass isoo ()
  ((name :reader name :initform "isoo")
   (iso :initarg :iso :reader iso)
   (output :initarg :output :reader output)
   (output-format :initarg :output-format :reader output-format)
   (arguments :initarg :arguments :reader arguments)
   (yes :initarg :yes :reader yes)
   (command-line-spec :reader command-line-spec :initform nil)))

(defmethod initialize-instance :after ((isoo isoo) &key)
  (command-line-arguments:handle-command-line
   (command-line-spec isoo)
   (handle-arguments isoo)
   :command-line (arguments isoo)
   :name (name isoo)))

(defgeneric handle-arguments (isoo)
  (:documentation "Handle the command line arguments."))

(defgeneric build (isoo)
  (:documentation "Build the root filesystem image."))

(defgeneric create-temporary-image (isoo image)
  (:documentation "Create the temporary image that the virtual machine will use."))

(defgeneric start-virtual-machine (isoo image &key)
  (:documentation "Start the virtual machine bootstrapped on the ISO."))

(defgeneric create-archive (isoo image)
  (:documentation "Create a useful archive of the image.ss"))

(defun load-all ()
  "Load all the .isoo files (really, Lisp files) on various places in
  the filesystem, leaving the user and system on where to put them."
  (let ((isoos-pathname (make-pathname :directory '(:relative "isoo")
                                       :name :wild
                                       :type "isoo")))

    (map nil #'load (remove-if-not
                     (lambda (base)
                       (directory (merge-pathnames isoos-pathname base)))
                     (uiop:xdg-config-pathnames)))))

(defmethod handle-arguments ((isoo isoo))
  (let ()
    (lambda (&key))))

(defmethod build ((isoo isoo))
  (with-temporary-directory (tmp)
    (let ((image (namestring (merge-pathnames "image" tmp))))
      (create-temporary-image isoo image)

      (start-virtual-machine isoo image)

      (unless (yes isoo)
        (format t "Do you want to make an archive of it? (y/n) ")
        (force-output)
        (unless (string= (read-line) "y")
          (format t "No problem, I'll be waiting for your orders.~%")
          (uiop:quit 0)))

      (create-archive isoo image))))

(defmethod create-temporary-image ((isoo isoo) image)
  (format t "Creating temporary image...~%")
  (uiop:run-program (list "qemu-img" "create" image "50G")))

(defmethod start-virtual-machine ((isoo isoo) image &key (additional-parameters nil))
  (let ((has-kvm (probe-file #P"/dev/kvm")))
    (format t (cat "isoostrap is about to start the virtual machine. "
                   "Please install your OS on one partition.~%"))

    (unless (yes isoo)
      (format t "Press [Enter] to continue.~%")
      (read-line))

    (let ((arguments
           `("qemu-system-x86_64"
             ,@(if has-kvm '("-enable-kvm" "-cpu" "host") '(""))
             ;; Too little iznogoud, too much hurts. Use my pinky's
             ;; choice.
             "-m" "512"
             ;; An empirically appropriate number.
             "-smp" ,(write-to-string (floor (/ (cpus-count) 2)))
             "-cdrom" ,(iso isoo)
             "-boot" "d"
             "-drive" ,(format nil "file=~A,media=disk,format=raw" image))))
      (when additional-parameters
        (setf arguments (append arguments additional-parameters)))
      (uiop:run-program arguments))

    (format t "Thank you for creating the base filesystem!~%")))

(defmethod create-archive ((isoo isoo) image)
  (with-temporary-directory (mounted-root)
    (with-mounted-loop-device (image mounted-root)
      (format t (cat "Creating the archive of the root filesystem. "
                     "Take a break, it's going to take a while.~%"))
      (uiop:run-program (list "tar"
                              (cat "-c"
                                   (second
                                    (find-if (lambda (x)
                                               (string= (first x) (output-format isoo)))
                                             *supported-formats*))
                                   "f")
                              (namestring (merge-pathnames (output isoo) (uiop:getcwd)))
                              "-C" (namestring mounted-root)
                              "."))
      (format t "All done! Your archive is ready at ~a.~%" (output isoo)))))
