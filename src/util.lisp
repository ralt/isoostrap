(defpackage #:isoostrap/util
  (:use #:cl)
  (:export cat
           cpus-count
           geteuid
           help
           http-response
           read-iso-label
           return-program-output
           starts-with
           with-mounted-iso
           with-mounted-loop-device
           with-ip-address
           with-hash-lock
           with-temporary-directory))

(in-package #:isoostrap/util)

(defun cat (&rest args)
  (apply #'concatenate 'string args))

(cffi:defcfun ("mkdtemp" %mkdtemp) :string
  (template :string))

(defun mkdtemp ()
  (pathname
   (cat
    (%mkdtemp
     (namestring
      (merge-pathnames "isoostrap.XXXXXX" (uiop:getcwd))))
    "/")))

(defmacro with-temporary-directory ((variable) &body body)
  `(let ((,variable (mkdtemp)))
     (unwind-protect
          (progn ,@body)
       (handler-case
           (uiop:delete-directory-tree ,variable
                                       :validate t)
         (error (e) (format t "Ignorable error: ~a~%" e))))))

(defconstant +sc-nprocessors-onln+ 84)

(cffi:defcfun "sysconf" :long
  (name :int))

(defun cpus-count ()
  (sysconf +sc-nprocessors-onln+))

(defun return-program-output (command)
  (with-output-to-string (s)
    (uiop:run-program command :output s)
    s))

(defmacro with-mounted-iso ((iso-file mountpoint) &body body)
  `(progn
     (uiop:run-program (list "mount" "-o" "ro" ,iso-file (namestring ,mountpoint)))
     (unwind-protect
          (progn ,@body)
       (uiop:run-program (list "umount" (namestring ,mountpoint))))))

(defmacro with-mounted-loop-device ((backing-file mountpoint) &body body)
  (let ((loop-device (gensym)))
    `(let ((,loop-device (string-trim
                          '(#\Newline)
                          (return-program-output
                           (list "losetup" "--partscan" "--show" "--find" ,backing-file)))))
       (unwind-protect
            (progn
              (uiop:run-program (list "mount" (cat ,loop-device "p1") (namestring ,mountpoint)))
              (unwind-protect
                   (progn ,@body)
                (uiop:run-program (list "umount" (namestring ,mountpoint)))))
         (uiop:run-program (list "losetup" "--detach" ,loop-device))))))

(cffi:defcfun "geteuid" :int)

(defmacro with-ip-address ((interface ip-address) &body body)
  `(progn
     (uiop:run-program (list "ip" "addr" "add" ,ip-address "dev" ,interface)
                       :ignore-error-status t)
     (unwind-protect
          (progn ,@body)
       (uiop:run-program (list "ip" "addr" "del" ,ip-address "dev" ,interface)
                         :ignore-error-status t))))

(defun http-response (stream response)
  "Appropriately format an HTTP response."
  (let ((buffer (babel:string-to-octets
                 (format nil "HTTP/1.0 200 OK~c~cContent-Length: ~a~c~c~c~c~a"
                         #\Return #\Linefeed
                         (length response)
                         #\Return #\Linefeed #\Return #\Linefeed
                         response))))
    (write-sequence buffer stream)))

(defun read-iso-label (iso-file)
  (with-open-file (f iso-file :element-type 'character)
    ;; Magic
    (file-position f 32808)
    (let ((buf (make-array 32 :element-type 'character)))
      (read-sequence buf f)
      buf)))

(defvar *hash-lock* (bt:make-lock))

(defmacro with-hash-lock ((hash-table key) &body body)
  (let ((lock (gensym))
        (presentp (gensym)))
    `(progn
       (bt:with-lock-held (*hash-lock*)
         (multiple-value-bind (,lock ,presentp)
             (gethash ,key ,hash-table)
           (declare (ignore ,lock))
           (unless ,presentp
             (setf (gethash ,key ,hash-table) (list :counter 0 :lock (bt:make-lock))))
           (incf (getf (gethash ,key ,hash-table) :counter))))
       (unwind-protect
            (bt:with-lock-held ((getf (gethash ,key ,hash-table) :lock))
              ,@body)
         (bt:with-lock-held (*hash-lock*)
           (multiple-value-bind (,lock ,presentp)
               (gethash ,key ,hash-table)
             (declare (ignore ,presentp))
             (decf (getf ,lock :counter))
             (when (= 0 (getf ,lock :counter))
               (remhash ,key ,hash-table))))))))

(defun starts-with (haystack needle)
  (string= (subseq haystack 0 (length needle)) needle))

(defun help (string)
  "Wrap around the help with usage boilerplate."
  (format t "Usage: isoostrap [OPTION]...
Bootstrap ISOs like no tomorrow!

~a
Report bugs at <https://gitlab.com/ralt/isoostrap/issues>.
" string))
