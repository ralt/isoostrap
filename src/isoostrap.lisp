(defpackage #:isoostrap
  (:use #:cl #:isoostrap/util)
  (:export user-error))

(defpackage #:isoostrap/user
  (:use #:cl))

(in-package #:isoostrap)

(defvar *command-line-spec*
  '((("iso")
     :type string
     :documentation "The ISO file to boot on.")
    (("output")
     :type string
     :documentation "The file where to dump the built root filesystem.")
    (("output-format")
     :type string
     :optional t
     :documentation "The output format of the dump. Defaults to 'targz'.")
    (("isoo")
     :type string
     :optional t
     :documentation "The isoo to perform custom actions. (See manual for more information.)")
    (("yes")
     :type boolean
     :optional t
     :documentation "Non-interactive mode.")
    (("help")
     :type string
     :optional t
     :documentation "Show usage message.")))

(define-condition user-error (condition)
  ((message :initarg :message :reader message)))

(defun main (argv)
  (handler-case
      (command-line-arguments:handle-command-line
       *command-line-spec*
       #'inner-main
       :command-line (rest argv)
       :name "isoostrap"
       :rest-arity :rest)
    (user-error (e)
      (format t "~A~%" (message e))
      (uiop:quit 1))
    (error (e)
      (format t "fatal: ~A~%" e)
      (uiop:quit -1))))

(defun inner-main (&key iso output (output-format "targz") isoo yes help rest)
  (when help
    (return-from inner-main (isoostrap/util:help
                             (isoostrap/util:cat
                              (with-output-to-string (s)
                                (command-line-arguments:show-option-help
                                 *command-line-spec*
                                 :stream s)
                                s)
                              "
To get help about an isoo, use `isoostrap --isoo <isoo> -- --help`.
"))))

  (unless (member "--help" rest :test #'string=)
    (unless (= (geteuid) 0)
      (error 'user-error :message "You need to be root."))
    (unless iso
      (error 'user-error :message "--iso is required."))
    (unless (probe-file iso)
      (error 'user-error :message (format nil "File ~a does not exist" iso)))
    (unless output
      (error 'user-error :message "--output is required."))
    (unless (member output-format (mapcar #'first isoostrap/isoo:*supported-formats*) :test #'string=)
      (error 'user-error :message (format nil "Output format ~a is not supported" output-format))))

  (isoostrap/isoo:load-all)

  (let* ((isoo-class (find-class (intern (string-upcase isoo) (find-package "ISOOSTRAP/USER"))
                                 nil))
         (isoo-instance (make-instance (or (and isoo isoo-class) 'isoostrap/isoo:isoo)
                                       :iso iso
                                       :output output
                                       :output-format output-format
                                       :yes yes
                                       :arguments rest)))
    (isoostrap/isoo:build isoo-instance)))
